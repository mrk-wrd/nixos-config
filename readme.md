# My NixOS Config

## Hosts

A list of hostnames and which hardware they're associated with.  Used hostnames appear at the top of the table with some future names unallocated below.

|Hostname        | Manufacturer | Model             | Comments                     |
|----------------|--------------|-------------------|------------------------------|
|slioch          |Dell          |Inspiron 5378      |Personal laptop               |
|am-basteir      |              |                   |                              |
|aonach-mor      |              |                   |                              |
|beinn-ghlas     |              |                   |                              |
|cairn-toul      |              |                   |                              |
|chno-dearg      |              |                   |                              |
|creise          |              |                   |                              |
|gleouraich      |              |                   |                              |
|lochnagar       |              |                   |                              |
|meall-garbh     |              |                   |                              |
|sgor-gaoith     |              |                   |                              |
|stob-binnein    |              |                   |                              |

