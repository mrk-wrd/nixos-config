{ lib, config, pkgs, ... }: {

  options = {
    NAME.enable = lib.mkEnableOption "Enable and configure NAME";
  };

  config = lib.mkIf config.NAME.enable {
    programs.NAME = {
      enable = true;
    };
  };
}
