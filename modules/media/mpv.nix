{ lib, config, pkgs, ... }: {

  options = {
    mpv.enable = lib.mkEnableOption "Enable and configure mpv";
  };

  config = lib.mkIf config.mpv.enable {
    programs.mpv = {
      enable = true;
    };
  };
}
