{
  imports = [
    ./editors
    ./media
    ./programs
    ./services
    ./shell
  ];
}
