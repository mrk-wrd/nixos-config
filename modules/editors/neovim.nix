{ lib , config, pkgs , ... }: {

  options = {
    neovim.enable = lib.mkEnableOption "Enable and configure Neovim";
  };

  config = lib.mkIf config.neovim.enable {
    programs.neovim = 
    let
      toLua = str: "lua << EOF\n${str}\nEOF\n\n";
      toLuaFile = file: "lua << EOF\n${builtins.readFile file}\nEOF\n\n";
    in {
      enable = true;

      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;

      extraLuaConfig = ''
        ${builtins.readFile ./neovim-config/options.lua}
      '';

      plugins = with pkgs.vimPlugins; [
        nvim-tree-lua
	      telescope-fzf-native-nvim
	      nvim-lspconfig
	      neodev-nvim
	      nvim-cmp
	      cmp_luasnip
	      friendly-snippets
	      lualine-nvim
	      nvim-web-devicons
        vim-nix

        {
          plugin = telescope-nvim;
          config = toLuaFile ./neovim-config/plugins/telescope.lua;
        }
	      {
	         plugin = comment-nvim;
	         config = toLua "require(\"Comment\").setup()";
        }
        {
          plugin = vim-startify;
          #config = toLua "let g:startify_change_to_vcs_root = 0";
        }
	      {
           plugin = (nvim-treesitter.withPlugins (p: [
             p.tree-sitter-nix
             p.tree-sitter-vim
             p.tree-sitter-bash
             p.tree-sitter-lua
             p.tree-sitter-python
             p.tree-sitter-json
           ]));
           config = toLuaFile ./neovim-config/plugins/treesitter.lua;
        }
      ];
    };
  };
}
