vim.cmd("set number relativenumber")

vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")

vim.cmd("hi Normal ctermbg=NONE guibg=NONE")
vim.cmd("hi NonText ctermbg=NONE guibg=NONE")
