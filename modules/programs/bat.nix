{ lib , config, pkgs , ... }: {

  options = {
    bat.enable = lib.mkEnableOption "Enable and configure Bat";
  };

  config = lib.mkIf config.bat.enable {
    programs.bat = {
      enable = true;
    };
  };
}
