{ lib , config, pkgs , ... }: {

  options = {
    tmux.enable = lib.mkEnableOption "Enable and configure tmux";
  };

  config = lib.mkIf config.tmux.enable {
    programs.tmux = {
      enable = true;
      clock24 = true;
    };
  };
}
