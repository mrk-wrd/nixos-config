{lib, config, pkgs, ... }: {

  options = {
    starship.enable = lib.mkEnableOption "Enable and apply starship config";
  };

  config = lib.mkIf config.starship.enable {
  
    programs.starship = {
      enable = true;
    };
  };
}
