{ lib, config, pkgs, ... }: {

  options = {
    taskwarrior.enable = lib.mkEnableOption "Enable and configure taskwarrior";
  };

  config = lib.mkIf config.taskwarrior.enable {
    programs.taskwarrior3 = {
      enable = true;
    };
  };
}
