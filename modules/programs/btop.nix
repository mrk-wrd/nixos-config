{ lib, config, pkgs, ... }: {

  options = {
    btop.enable = lib.mkEnableOption "Enable and configure btop";
  };

  config = lib.mkIf config.btop.enable {
    programs.btop = {
      enable = true;
      settings = {
	      theme_background = false;
	      vim_keys = true;
      };
    };
  };
}
