{ lib, config, pkgs, ... }: {

  options = {
    firefox.enable = lib.mkEnableOption "Enable and apply Firefox configuration";
  };

  config = lib.mkIf config.firefox.enable {

    programs.firefox = {
      enable = true;
      policies = {
        "DisableFirefoxStudies" = true;
        "DisableFormHistory" = true;
        "DisableMasterPasswordCreation" = true;
        "DisablePocket" = true;
        "DisableTelemetry" = true;
        "DontCheckDefaultBrowser" = true;
        "EnableTrackingProtection" = {
          "Value" = true;
	        "Cryptomining" = true;
	        "Fingerprinting" = true;
    	    "EmailTracking" = true;
        };
        "PasswordManagerEnabled" = false;
        "PromptForDownloadLocation" = true;
      };
      profiles.mark = {
        #extensions = with pkgs.inputs.firefox-addons; [
        #  ublock-origin
        #  privacy-badger
        #];
        settings = {  };
      };
    };

    xdg.mimeApps.defaultApplications = {
      "text/html" = [ "firefox.desktop" ];
      "text/xml" = [ "firefox.desktop" ];
      "x-scheme-handler/http" = [ "firefox.desktop" ];
      "x-scheme-handler/https" = [ "firefox.desktop" ];
    };
  };
}
