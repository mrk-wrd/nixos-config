{ lib, config, pkgs, ... }: {

  options = {
    alacritty.enable = lib.mkEnableOption "Enable Alacritty terminal emulator";
  };

  config = lib.mkIf config.alacritty.enable {

    programs.alacritty = {
      enable = true;
      settings = {
        window = {
          decorations = "None";
          opacity = lib.mkForce 0.9;
          startup_mode = "Maximized";
          padding = {
            x  = 50;
            y = 50;
          };
        };
        scrolling.history = 10000;
      };
    };
  };    
}
