{ lib , config, pkgs , ... }: {

  options = {
    eza.enable = lib.mkEnableOption "Enable and configure Eza";
  };

  config = lib.mkIf config.eza.enable {
    programs.eza = {
      enable = true;
      git = true;
      icons = true;
    };
  };
}
