{
  imports = [
    ./alacritty.nix
    ./bat.nix
    ./btop.nix
    ./eza.nix
    ./firefox.nix
    ./kitty.nix
    ./starship.nix
    ./tmux.nix
  ];
}
