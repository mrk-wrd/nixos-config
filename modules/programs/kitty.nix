{ lib, config, pkgs, ... }: {

  options = {
    kitty.enable = lib.mkEnableOption "Enable Kitty terminal emulator";
  };

  config = lib.mkIf config.kitty.enable {

    programs.kitty = {
      enable = true;
      settings = {
        scrollback_lines = 10000;
        enable_audio_bell = false;
        update_check_interval = 0;
        window_padding_width = 40;
        hide_window_decorations = "yes";
      };
    };
  };    
}
