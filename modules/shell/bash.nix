{ lib , config, pkgs , ... }: {

  options = {
    bash.enable = lib.mkEnableOption "Enable Bash config";
  };

  config = lib.mkIf config.bash.enable {
    programs.bash = {
      enable = true;
      enableCompletion = true;

      shellAliases = {
        ls = "eza";
        ll = "eza -lh";
        la = "eza -lah";
      };
    };
  };
}
