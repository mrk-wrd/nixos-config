{ lib, config, pkgs, ... }: {

  options = {
    git.enable = lib.mkEnableOption "Enable and configure git";
  };

  config = lib.mkIf config.git.enable {
    programs.git = {
      enable = true;
      userName = "Mark";
      userEmail = "mark@merm.co.uk";
      extraConfig = {
        init.defaultBranch = "main";
      };
    };
  };
}
