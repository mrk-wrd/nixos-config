{ lib , config, pkgs , ... }: {

  options = {
    zsh.enable = lib.mkEnableOption "Enable and configure Zsh";
  };

  config = lib.mkIf config.zsh.enable {
    programs.zsh = {
      enable = true;
      enableCompletion = true;
      autosuggestion.enable = true;
      autocd = true;

      shellAliases = {
        ls = "eza";
        ll = "eza -lh";
        la = "eza -lah";
      };
    };
  };
}
