# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, inputs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      inputs.sops-nix.nixosModules.sops
      inputs.home-manager.nixosModules.default
      inputs.nixos-hardware.nixosModules.common-pc-laptop-ssd
      inputs.nixos-hardware.nixosModules.common-gpu-intel-kaby-lake
      inputs.stylix.nixosModules.stylix
    ];

  # Experimental features
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Unfree
  #allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [ "steam" ];
  nixpkgs.config.allowUnfree = true;

  # Time zone and 
  time.timeZone = "Europe/London";

  # I18N and Keyboard
  i18n.defaultLocale = "en_GB.UTF-8";
  console.keyMap = "uk";

  security.rtkit.enable = true;

  # Bootloader
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
      timeout = 2;
    };
    plymouth.enable = true;
  };

  # zram swap
  zramSwap.enable = true;

  # Networking
  networking.hostName = "slioch";
  networking.networkmanager.enable = true; 

  # Virtualisation
  virtualisation.libvirtd.enable = true;

  # Services
  services = {
    openssh.enable = true;
    printing.enable = true;
    libinput.enable = true; # Touchpad
    fwupd.enable = true;
    xserver = { # Display Manager
      enable = true;
      xkb.layout = "gb";
      displayManager.gdm.enable = true;
      desktopManager.gnome.enable = true;
    };
    pipewire = { # Sound & Audio
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    mullvad-vpn = {
      enable = true;
      package = pkgs.mullvad-vpn;
    };
  };

  # Programs
  programs = {
    zsh.enable = true;
    virt-manager.enable = true;
    nh = {
      enable = true;
      clean.enable = true;
      clean.extraArgs = "--keep-since 14d --keep=5";
      flake = /home/mark/.dotfiles;
    };
    steam = {
      enable = true;
      gamescopeSession.enable = true;
    };
    gamemode.enable = true;
  };

  # SOPS
  sops = {
    defaultSopsFile = ../../secrets.yaml;
    defaultSopsFormat = "yaml";

    age = {
      #sshKeyPaths = [ /etc/ssh/ssh_host_ed25519_key ];
      #keyFile = /var/lib/sops-nix/key.txt;
      #generateKey = true;
      keyFile = /home/mark/.config/sops/age/keys.txt;
    };

    secrets = {
      "hosts/slioch/ssh/ssh_host_ed25519_key" = {
        path = "/etc/ssh/ssh_host_ed25519_key";
      };
      "hosts/slioch/ssh/ssh_host_ed25519_key.pub" = {
        path = "/etc/ssh/ssh_host_ed25519_key.pub";
      };
      "hosts/slioch/ssh/ssh_host_rsa_key" = {
        path = "/etc/ssh/ssh_host_rsa_key";
      };
      "hosts/slioch/ssh/ssh_host_rsa_key.pub" = {
        path = "/etc/ssh/ssh_host_rsa_key.pub";
      };
      "users/mark/ssh/id_ed25519" = {
      path = "/home/mark/.ssh/id_ed25519";
        owner = config.users.users.mark.name;
        group = config.users.users.mark.group;
        mode = "0600";
      };
      "users/mark/ssh/id_ed25519.pub" = {
        path = "/home/mark/.ssh/id_ed25519.pub";
        owner = config.users.users.mark.name;
        group = config.users.users.mark.group;
        mode = "0744";
      };
    };
  };

  # Users
  sops.secrets."users/mark/password".neededForUsers = true;
  users.mutableUsers = false;
  users.users.mark = {
    isNormalUser = true;
    hashedPasswordFile = config.sops.secrets."users/mark/password".path;
    extraGroups = [ "wheel" "networkmanager" "libvirtd" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.zsh;
  };

  # Home-Manager
  home-manager = {
    extraSpecialArgs = {inherit inputs; };
    users.mark = import ./home.nix;
  };
  
  # System Default Packages
  environment.systemPackages = with pkgs; [
    neovim
    git
    sops
    age
  ];

  stylix = {
    enable = true;
    polarity = "dark";
    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-mocha.yaml";
    image = ../../modules/wallpapers/langdale.jpg;
    cursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
    };
    fonts = {
      monospace = {
        package = pkgs.nerdfonts.override {fonts = [ "FiraCode" ]; };
        name = "FiraCode Nerd Font Mono";
      };
      sansSerif = {
        package = pkgs.dejavu_fonts;
        name = "DejaVu Sans";
      };
      serif = {
        package = pkgs.dejavu_fonts;
        name = "DejaVua Serif";
      };
    };    
  };




  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

